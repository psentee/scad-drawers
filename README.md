Parametric modular drawers
==========================

Programmatic inserts for the [modular drawers
remix](https://www.thingiverse.com/thing:3154217) model. I liked the
drawers and modules, but wanted to have different, specialized
inserts: divided in various ways, with nozzle or tool organizers, with
rolls for SMT elements, with Eppendorf tube holders, etc.

WIP, obviously. CC BY-SA.
